package counter.propagation.network;

import java.util.Arrays;

/**
 *
 * @author Umberto
 */
public class CounterPropagationNetwork {

    private KohonenLayer kohonen;
    private GrossbergLayer grossberg;

    public CounterPropagationNetwork(KohonenLayer k, GrossbergLayer g) {
        this.grossberg = g;
        this.kohonen = k;
    }

    public void test(double[][] input, int[] expectedClassKohonen, int[][] expectedOutputGrossberg) {
        int[][] outputK = kohonen.test(input, expectedClassKohonen);
        grossberg.test(outputK, expectedOutputGrossberg);
    }

    private int MAX_NOISE_BITS = 16;

    public void robustnessTest(int[][] input, int[] expectedClassKohonen, int[][] expectedOutputGrossberg) {
        int[] success = new int[input.length];
        for (int j = 0; j < MAX_NOISE_BITS; j++) {
            System.out.printf("\nROBUSTNESS TEST, noise bits:%d\n", j);
            Arrays.fill(success, 0); //reset success
            for (int i = 0; i < 100; i++) {
                //normalize test input and add noise
                double[][] normalizedNoiseTestInput = new double[input.length][input[0].length];
                for (int h = 0; h < input.length; h++) {
                    normalizedNoiseTestInput[h] = Utility.
                            arrayNormalize(Utility.
                                    insertNoise(Utility.extractRow(input, h), j));
                }
                //test robustness 

                int[][] outputK = kohonen.
                        test(normalizedNoiseTestInput, expectedClassKohonen);
                success = Utility.arraySum(grossberg.
                        test(outputK, expectedOutputGrossberg), success);
            }
            for (int i = 0; i < input.length; i++) {
                System.out.
                        printf("Input %d : %d%%\n", i, success[i]);
            }
        }
    }

}
