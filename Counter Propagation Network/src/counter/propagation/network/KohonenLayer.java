/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package counter.propagation.network;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Umberto
 */
public class KohonenLayer {

    private int numberOfNeurons;
    private Map<Integer, Boolean> hasBeenInitialized;
    private double[] weights; //weights of the kohoen layer in an array
    private double[] z; //value of w*x for a single neuron i.e. output of summation
    private int[][] y; //activation WTA
    private int numberOfInputs;
    private double ALPHA = 0.1; //learning coefficent for weights

    public KohonenLayer(int numberOfInputs, int numberOfNeurons) {
        this.numberOfNeurons = numberOfNeurons;
        //Each neuron will have its weights and I want to know if they have been 
        //already initialized
        this.hasBeenInitialized = new HashMap<>();

        //create array for weights, for each neuron there are numberOfInputs
        //weights
        weights = new double[numberOfNeurons * numberOfInputs];

        this.numberOfInputs = numberOfInputs;
        z = new double[numberOfNeurons];

    }

    /**
     * ATTENTION: the classOfInput should be progressive starting from 0, so if
     * there are three classes the available value for classOfInputs are
     * integers 0,1 and 2. This is a trick and may be made more user-friendly in
     * next versions
     *
     * @param input A matrix with all the inputs as vectors. e.g. if I have 3
     * inputs that arer vectors of 64 bits than input will be [3][64]
     * @param classOfInput the corresponding class for each input e.g. in this
     * case [3]
     */
    public void train(double[][] input, int[] classOfInput) {
        //for each training input
        for (int i = 0; i < classOfInput.length; i++) {
            //if the weights for that class have not been initialized
            //so there's no entry for that class in the hashmap
            //than use the input vector to initialize the weights
            if (hasBeenInitialized.get(classOfInput[i]) == null) {
                hasBeenInitialized.put(classOfInput[i], Boolean.TRUE); //save initialization
                double[] currentRow = Utility.extractRow(input, i);
                System.
                        arraycopy(currentRow, 0, this.weights, numberOfInputs * classOfInput[i], numberOfInputs);
            } else {
                //if the weights were initialized (i.e. there are more than one training 
                //examples for that class)
                //adjust the weights
                double[] currentRow = Utility.extractRow(input, i);
                //System.out.println("new vector:");
                //Utility.printVector(currentRow);

                double[] subWeight = Arrays.
                        copyOfRange(weights, numberOfInputs * classOfInput[i], numberOfInputs * classOfInput[i] + numberOfInputs);
                //System.out.println("old vector:");

                //Utility.printVector(subWeight);
                double[] delta = Utility.arrayDiff(currentRow, subWeight);
                //System.out.println("diff:");

                //Utility.printVector(delta);
                //System.out.println("mult:");
                //Utility.printVector(Utility.arrayMultiplyScalar(delta, ALPHA));
                //update weights w(n+1) = w(n) + alpha* (input-weights)
                double[] newSubWeights = Utility.arraySum(Utility.
                        arrayMultiplyScalar(delta, ALPHA), subWeight);
                //Utility.printVector(newSubWeights);

                newSubWeights = Utility.arrayNormalize(newSubWeights); //re-normalize vector
                //System.out.println("re-normalized");
                //Utility.printVector(newSubWeights);

                System.
                        arraycopy(newSubWeights, 0, weights, numberOfInputs * classOfInput[i], numberOfInputs);
            }
            //System.out.println("final");
            //Utility.printVector(weights, numberOfInputs);
        }
        //Utility.printVector(weights, numberOfInputs);
    }

    public int[][] test(double[][] input, int[] classOfInput) {
        y = new int[classOfInput.length][numberOfNeurons];
        if (input[0].length != numberOfInputs) {
            System.out.
                    printf("Expected input  were %d received are %d, abort.\n", numberOfInputs, input[0].length);
            return null;
        }
        for (int k = 0; k < classOfInput.length; k++) {
            //feedforward
            double max = -1;
            double index = 0;
            //System.out.printf("Class %d\n", k);
            for (int i = 0; i < numberOfNeurons; i++) {
                z[i] = 0;

                for (int j = 0; j < numberOfInputs; j++) {
                    //System.out.printf("%f * %f +",weights[i * numberOfInputs + j],input[k][j]);
                    z[i] += weights[i * numberOfInputs + j] * input[k][j];
                }
                if (max < z[i]) {
                    max = z[i];
                    index = i;
                }
                //System.out.printf("\n%d: %f\n", i, z[i]);
            }
            //activate layer
            for (int i = 0; i < numberOfNeurons; i++) {
                y[k][i] = (i == index) ? 1 : 0;
                //System.out.printf("\n%d: %d\n", i, y[k][i]);

            }
        }
        return y;
    }

}
