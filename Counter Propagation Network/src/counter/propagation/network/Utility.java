/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package counter.propagation.network;

import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

/**
 *
 * @author Umberto
 */
public class Utility {

    public static void printVector(int[] vector) {
        for (int i = 0; i < vector.length; i++) {
            System.out.printf("[%d]", vector[i]);
        }
        System.out.println("");
    }

    public static void printVector(double[] vector) {
        for (int i = 0; i < vector.length; i++) {
            System.out.printf("[%.2f]", vector[i]);
        }
        System.out.println("\n");
    }

    public static void printVector(double[] vector, int each) {
        for (int i = 0; i < vector.length; i++) {
            System.out.printf("[%.2f]", vector[i]);
            if ((i + 1) % each == 0) {
                System.out.println("");
            }
        }
        System.out.println("");
    }

    public static double[] extractRow(double[][] dataset, int row) {
        //System.out.println("Extracting row...\n");
        double[] tmpArray = new double[dataset[row].length];
        System.arraycopy(dataset[row], 0, tmpArray, 0, dataset[row].length);
        return tmpArray;
    }

    public static int[] extractRow(int[][] dataset, int row) {
        //System.out.println("Extracting row...\n");
        int[] tmpArray = new int[dataset[row].length];
        System.arraycopy(dataset[row], 0, tmpArray, 0, dataset[row].length);
        return tmpArray;
    }

    /**
     * result = a-b
     *
     * @param result
     * @param a
     * @param b
     * @return
     */
    public static double[] arrayDiff(double[] a, double[] b) {
        if (a.length != b.length) {
            System.out.println("Arrays have different lenghts!");
        }
        double[] tmp = new double[Math.min(a.length, b.length)];
        for (int i = 0; i < Math.min(a.length, b.length); i++) {
            tmp[i] = a[i] - b[i];
        }
        return tmp;
    }

    public static double[] arraySum(double[] a, double[] b) {
        if (a.length != b.length) {
            System.out.println("Arrays have different lenghts!");
        }
        double[] tmp = new double[Math.min(a.length, b.length)];
        for (int i = 0; i < Math.min(a.length, b.length); i++) {
            tmp[i] = a[i] + b[i];
        }
        return tmp;
    }

    public static int[] arraySum(int[] a, int[] b) {
        if (a.length != b.length) {
            System.out.println("Arrays have different lenghts!");
        }
        int[] tmp = new int[Math.min(a.length, b.length)];
        for (int i = 0; i < Math.min(a.length, b.length); i++) {
            tmp[i] = a[i] + b[i];
        }
        return tmp;
    }

    public static double[] arrayMultiplyScalar(double[] a, double coefficent) {
        double[] tmp = new double[a.length];
        for (int i = 0; i < a.length; i++) {
            tmp[i] = a[i] * coefficent;
        }
        return tmp;
    }

    public static double[] arrayNormalize(double[] array) {
        //calculate sum of squares / lenght of array
        double sum = DoubleStream.of(array).map(n -> n * n).sum();
        sum = Math.sqrt(sum);

        double[] tmp = new double[array.length];
        for (int i = 0; i < array.length; i++) {
            tmp[i] = array[i] / sum;
        }
        return tmp;
    }

    public static double[] arrayNormalize(int[] array) {
        //calculate sum of squares / lenght of array
        double sum = IntStream.of(array).map(n -> n * n).sum();
        sum = Math.sqrt(sum);

        double[] tmp = new double[array.length];
        for (int i = 0; i < array.length; i++) {
            tmp[i] = array[i] / sum;
        }
        return tmp;
    }

    public static int[] insertNoise(int[] input, int numberOfNoiseBits) {
        int[] tmp = new int[input.length];
        System.arraycopy(input, 0, tmp, 0, input.length);

        int index;
        //for each noise bit to itroduce
        for (int i = 0; i < numberOfNoiseBits; i++) {
            //select one random bit in the vector and reverse it
            index = RandomGenerator.randomInRange(0, input.length - 1);
            tmp[index] = (tmp[index] == 0) ? 1 : 0; //reverse the bit
        }
        return tmp;
    }
}
