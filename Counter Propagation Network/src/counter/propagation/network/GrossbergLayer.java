/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package counter.propagation.network;

import java.util.Arrays;

/**
 *
 * @author Umberto
 */
public class GrossbergLayer {

    private int numberOfInputs;
    private int numberOfOutputs;
    private int z[]; //output values
    private int[] weights;

    public GrossbergLayer(int numberOfInputs, int numberOfOutputs) {
        this.numberOfInputs = numberOfInputs;
        this.numberOfOutputs = numberOfOutputs;
        this.weights = new int[numberOfOutputs * numberOfInputs];
        this.z = new int[numberOfOutputs];
    }

    public void train() {
        //since each output of the kohonen is translated in a single output in
        //the grossberg layer, I just need to set the weigths to the ouput values
        weights = new int[]{0, 1, 0, 1, 0, 0, 1, 1};
    }

    public int[] test(int[][] input, int[][] expectedOutputGrossberg) {
        int[] success = new int[input.length];
        if (input[0].length != numberOfInputs) {
            System.out.
                    printf("Expected input  were %d received are %d, abort.\n", numberOfInputs, input[0].length);
            return null;
        }
        for (int k = 0; k < input.length; k++) {
            //feedforward
            //System.out.printf("Class %d\n", k);
           // System.out.printf("Input %d:", k);
            for (int i = 0; i < numberOfOutputs; i++) {
                z[i] = 0;

                for (int j = 0; j < numberOfInputs; j++) {
                    z[i] += weights[i * numberOfInputs + j] * input[k][j];
                }
            }
            //check if class match
            if (Arrays.equals(z, expectedOutputGrossberg[k])) {
              //  System.out.println("-RECOGNIZED-");
                success[k] = 1;
            } else {
              //  System.out.println("--NOT RECOGNIZED");
                success[k] = 0;

            }
        }
        return success;
    }
}
