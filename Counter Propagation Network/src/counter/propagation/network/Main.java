package counter.propagation.network;

/**
 *
 * @author Umberto
 */
public class Main {

    private static int[][] inputTrain
            = {{0, 0, 0, 1, 1, 0, 0, 0,
                0, 0, 1, 0, 0, 1, 0, 0, // ‘A’
                0, 1, 0, 0, 0, 0, 1, 0,
                1, 0, 0, 0, 0, 0, 0, 1,
                1, 1, 1, 1, 1, 1, 1, 1,
                1, 0, 0, 0, 0, 0, 0, 1,
                1, 0, 0, 0, 0, 0, 0, 1,
                1, 0, 0, 0, 0, 0, 0, 1},
               {1, 1, 1, 1, 1, 1, 1, 0,
                1, 0, 0, 0, 0, 0, 0, 1,// ‘B’
                1, 0, 0, 0, 0, 0, 1, 0,
                1, 1, 1, 1, 1, 1, 0, 0,
                1, 0, 0, 0, 0, 0, 1, 0,
                1, 0, 0, 0, 0, 0, 0, 1,
                1, 0, 0, 0, 0, 0, 1, 0,
                1, 1, 1, 1, 1, 1, 0, 0},
               {0, 0, 1, 1, 1, 1, 1, 1, // ‘C’
                0, 1, 0, 0, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0, 0, 0,
                0, 1, 0, 0, 0, 0, 0, 0,
                0, 0, 1, 1, 1, 1, 1, 1},
               {1, 1, 1, 1, 1, 1, 0, 0, // ‘D’
                1, 0, 0, 0, 0, 0, 1, 0,
                1, 0, 0, 0, 0, 0, 0, 1,
                1, 0, 0, 0, 0, 0, 0, 1,
                1, 0, 0, 0, 0, 0, 0, 1,
                1, 0, 0, 0, 0, 0, 0, 1,
                1, 0, 0, 0, 0, 0, 1, 0,
                1, 1, 1, 1, 1, 1, 0, 0},
               {1, 1, 1, 1, 1, 1, 1, 1, // ‘E’
                1, 0, 0, 0, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0, 0, 0,
                1, 1, 1, 1, 1, 1, 1, 1,
                1, 0, 0, 0, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0, 0, 0,
                1, 1, 1, 1, 1, 1, 1, 1},
               {1, 1, 1, 1, 1, 1, 1, 1,// ‘F’
                1, 0, 0, 0, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0, 0, 0,
                1, 1, 1, 1, 1, 1, 1, 1,
                1, 0, 0, 0, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0, 0, 0}

            };

    private static int[][] inputTest = {
        {0, 0, 0, 1, 1, 0, 0, 0,
         0, 0, 1, 0, 0, 1, 0, 0, // ‘A’
         0, 1, 0, 0, 0, 0, 1, 0,
         1, 0, 0, 0, 0, 0, 0, 1,
         1, 1, 1, 1, 1, 1, 1, 1,
         1, 0, 0, 0, 0, 0, 0, 1,
         1, 0, 0, 0, 0, 0, 0, 1,
         1, 0, 0, 0, 0, 0, 0, 1},
        {1, 1, 1, 1, 1, 1, 1, 0,
         1, 0, 0, 0, 0, 0, 0, 1,// ‘B’
         1, 0, 0, 0, 0, 0, 1, 0,
         1, 1, 1, 1, 1, 1, 0, 0,
         1, 0, 0, 0, 0, 0, 1, 0,
         1, 0, 0, 0, 0, 0, 0, 1,
         1, 0, 0, 0, 0, 0, 1, 0,
         1, 1, 1, 1, 1, 1, 0, 0},
        {0, 0, 1, 1, 1, 1, 1, 1, // ‘C’
         0, 1, 0, 0, 0, 0, 0, 0,
         1, 0, 0, 0, 0, 0, 0, 0,
         1, 0, 0, 0, 0, 0, 0, 0,
         1, 0, 0, 0, 0, 0, 0, 0,
         1, 0, 0, 0, 0, 0, 0, 0,
         0, 1, 0, 0, 0, 0, 0, 0,
         0, 0, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 0, 0, // ‘D’
         1, 0, 0, 0, 0, 0, 1, 0,
         1, 0, 0, 0, 0, 0, 0, 1,
         1, 0, 0, 0, 0, 0, 0, 1,
         1, 0, 0, 0, 0, 0, 0, 1,
         1, 0, 0, 0, 0, 0, 0, 1,
         1, 0, 0, 0, 0, 0, 1, 0,
         1, 1, 1, 1, 1, 1, 0, 0},
        {1, 1, 1, 1, 1, 1, 1, 1, // ‘E’
         1, 0, 0, 0, 0, 0, 0, 0,
         1, 0, 0, 0, 0, 0, 0, 0,
         1, 1, 1, 1, 1, 1, 1, 1,
         1, 0, 0, 0, 0, 0, 0, 0,
         1, 0, 0, 0, 0, 0, 0, 0,
         1, 0, 0, 0, 0, 0, 0, 0,
         1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1,// ‘F’
         1, 0, 0, 0, 0, 0, 0, 0,
         1, 0, 0, 0, 0, 0, 0, 0,
         1, 1, 1, 1, 1, 1, 1, 1,
         1, 0, 0, 0, 0, 0, 0, 0,
         1, 0, 0, 0, 0, 0, 0, 0,
         1, 0, 0, 0, 0, 0, 0, 0,
         1, 0, 0, 0, 0, 0, 0, 0},
        {1, 0, 0, 0, 0, 0, 0, 1,
         1, 0, 0, 0, 0, 0, 0, 1, // ‘H’
         1, 0, 0, 0, 0, 0, 0, 1,
         1, 1, 1, 1, 1, 1, 1, 1,
         1, 0, 0, 0, 0, 0, 0, 1,
         1, 0, 0, 0, 0, 0, 0, 1,
         1, 0, 0, 0, 0, 0, 0, 1,
         1, 0, 0, 0, 0, 0, 0, 1},
        {0, 1, 0, 0, 0, 0, 0, 1,
         0, 0, 1, 0, 0, 0, 1, 0, // ‘Y’
         0, 0, 0, 1, 0, 1, 0, 0,
         0, 0, 0, 0, 1, 0, 0, 0,
         0, 0, 0, 0, 1, 0, 0, 0,
         0, 0, 0, 0, 1, 0, 0, 0,
         0, 0, 0, 0, 1, 0, 0, 0,
         0, 0, 0, 0, 1, 0, 0, 0},
        {1, 0, 0, 0, 0, 0, 0, 1,
         0, 1, 0, 0, 0, 0, 1, 0, // ‘X’
         0, 0, 1, 0, 0, 1, 0, 0,
         0, 0, 0, 1, 1, 0, 0, 0,
         0, 0, 0, 1, 1, 0, 0, 0,
         0, 0, 1, 0, 0, 1, 0, 0,
         0, 1, 0, 0, 0, 0, 1, 0,
         1, 0, 0, 0, 0, 0, 0, 1}
    };

    private static int[] classOfTrainInput = {0, 1, 2, 3, 3, 3};
    private static int[] classOfTestInput = {0, 1, 2, 3, 3, 3, 3, 3, 3};
    private static int[][] expectedOutputGrossberg
            = {{0, 0}, {1, 0}, {0, 1}, {1, 1}, {1, 1}, {1, 1}, {1, 1}, {1, 1}, {1, 1}};

    private static int NUMBER_OF_INPUTS = 64;
    private static int NUMBER_OF_NEURONS_KOHONEN = 4; //classes
    private static int NUMBER_OF_TRAINING_SAMPLES = 6;
    private static int NUMBER_OF_TEST_SAMPLES = 9;
    private static int NUMBER_OF_NEURONS_GROSSBERG = 2;

    private static int TRAIN_EPOCHS = 10;

    public static void main(String[] args) {

        //create kohonen layer
        KohonenLayer kohonenLayer = new KohonenLayer(NUMBER_OF_INPUTS, NUMBER_OF_NEURONS_KOHONEN);

        //normalize train input
        double[][] normalizedTrainInput = new double[NUMBER_OF_TRAINING_SAMPLES][NUMBER_OF_INPUTS];

        for (int i = 0; i < NUMBER_OF_TRAINING_SAMPLES; i++) {
            normalizedTrainInput[i] = Utility.arrayNormalize(Utility.
                    extractRow(inputTrain, i));
        }

        //train cohonen
        for (int i = 0; i < TRAIN_EPOCHS; i++) {
            kohonenLayer.train(normalizedTrainInput, classOfTrainInput);
        }

        //create Grossberg layer
        GrossbergLayer grossberg = new GrossbergLayer(NUMBER_OF_NEURONS_KOHONEN, NUMBER_OF_NEURONS_GROSSBERG);

        //train Grossberg layer
        grossberg.train();

        //create counter propagation net with the two layers
        CounterPropagationNetwork net = new CounterPropagationNetwork(kohonenLayer, grossberg);

        //TEST NET
        //normalize test input
        double[][] normalizedTestInput = new double[NUMBER_OF_TEST_SAMPLES][NUMBER_OF_INPUTS];
        for (int i = 0; i < NUMBER_OF_TEST_SAMPLES; i++) {
            normalizedTestInput[i] = Utility.arrayNormalize(Utility.
                    extractRow(inputTest, i));
        }
        //test 
        System.out.println("TEST");
        net.test(normalizedTestInput, classOfTestInput, expectedOutputGrossberg);

        //TEST ROBUSTNESS
        net.
                robustnessTest(inputTest, classOfTestInput, expectedOutputGrossberg);
        
        gatherStats(kohonenLayer);

    }

    private static void gatherStats(KohonenLayer net) {
        int NUMB_OF_RUNS = 1000;
        long sum = 0;
        //normalize train input
        double[][] normalizedTrainInput = new double[NUMBER_OF_TRAINING_SAMPLES][NUMBER_OF_INPUTS];

        for (int i = 0; i < NUMBER_OF_TRAINING_SAMPLES; i++) {
            normalizedTrainInput[i] = Utility.arrayNormalize(Utility.
                    extractRow(inputTrain, i));
        }
        for (int i = 0; i < NUMB_OF_RUNS; i++) {
            long startTime = System.currentTimeMillis();
            //train cohonen
            for (int j = 0; j < TRAIN_EPOCHS; j++) {
                net.train(normalizedTrainInput, classOfTrainInput);
            }
            long endTime = System.currentTimeMillis();
            long totalTime = endTime - startTime;
            sum += totalTime;
        }
        System.out.
                printf("Execution time for %d trainings each one of %d epochs is: %f milliseconds\n", NUMB_OF_RUNS, TRAIN_EPOCHS,(float) sum / NUMB_OF_RUNS);
    }
}
